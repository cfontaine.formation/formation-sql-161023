USE exemple;

-- CREATE TEMPORARY TABLE -> table temporaire
-- Elle sera supprimée dés que l'on quitte la session
CREATE TEMPORARY TABLE utilisateurs
(
	id INT PRIMARY KEY AUTO_INCREMENT,
	email VARCHAR(150) NOT NULL,
	password VARCHAR(40) NOT NULL
);

INSERT INTO utilisateurs VALUES
(1,'jd@dawan.fr','dawan'),
(2,'mr@dawan.fr','1234'),
(3,'nr@dawan.fr','12345');

-- affiche tous les utilisateur
SELECT * FROM utilisateurs;

-- On se deconnecte du SGBD, la table n'éxiste plus
SELECT * FROM utilisateurs;

-- TABLE CTE -> table à usage unique
USE bibliotheque;

WITH auteur_vivant_cte AS
(
	SELECT auteurs.id AS id_auteur,prenom AS auteur_prenom, auteurs.nom AS auteur_nom, naissance, pays.nom FROM auteurs 
	INNER JOIN pays ON pays.id=auteurs.nation
	WHERE deces IS NULL
)-- La table CTE doit être utilisée immédiatement après sa déclaration, sinon cela provoque une erreur
SELECT  auteur_prenom, auteur_nom,naissance FROM auteur_vivant_cte WHERE naissance > '1950-01-01';
-- SELECT * FROM auteur_vivant_usa_cte; -- erreur


WITH auteur_vivant_cte AS
(
	SELECT auteurs.id AS id_auteur,prenom AS auteur_prenom, auteurs.nom AS auteur_nom, naissance, pays.nom AS nom_pays FROM auteurs 
	INNER JOIN pays ON pays.id=auteurs.nation
	WHERE deces IS NULL
),
auteur_vivant_usa_cte AS -- On peut déclarer plusieurs table CTE à la suite avant de les utiliser
(
	SELECT id_auteur,  auteur_prenom , auteur_nom ,nom_pays FROM auteur_vivant_cte WHERE nom_pays='États-Unis'
)
SELECT auteur_prenom , auteur_nom ,nom_pays FROM auteur_vivant_usa_cte;

-- Création d'une table à partir d'une sélection
CREATE TEMPORARY TABLE backup_livres SELECT * FROM livres;

SELECT * FROM backup_livres;

CREATE TABLE livres_policier SELECT * FROM livres WHERE genre=1;

