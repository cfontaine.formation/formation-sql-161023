USE bibliotheque;

-- Création d'une vue
CREATE VIEW v_auteur_livre AS
SELECT livres.id, titre, concat_ws(' ',auteurs.prenom,auteurs.nom) AS auteur,
genres.nom AS genre_livre
FROM livres
INNER JOIN genres ON livres.genre=genres.id
INNER JOIN livre2auteur ON livre2auteur.id_livre=livres.id 
INNER JOIN auteurs ON livre2auteur.id_auteur=auteurs .id;

-- Avec show table, une vue sera considérée comme une table
SHOW tables;

-- On peut exécuter des requêtes sur la vue
SELECT * FROM v_auteur_livre 
WHERE genre_livre ='Drame';

-- Si on ajoute des données dans la vue
INSERT INTO livres(titre,annee,genre)VALUES ('Les misérables',1862,7);
INSERT INTO livre2auteur(id_livre,id_auteur)VALUES(143,33); 

-- Elle mise à jour dynamiquement
SELECT * FROM v_auteur_livre 
WHERE genre_livre ='Drame';

-- ALTER VIEW -> Modifier une vue (recréation)
ALTER VIEW v_auteur_livre  AS 
SELECT livres.id, titre, concat_ws(' ',auteurs.prenom,auteurs.nom) AS auteur,
genres.nom  AS genre_livre,pays.nom AS pays
FROM livres
INNER JOIN genres ON livres.genre=genres.id
INNER JOIN livre2auteur ON livre2auteur.id_livre=livres.id 
INNER JOIN auteurs ON livre2auteur.id_auteur=auteurs.id
INNER JOIN pays ON nation=pays.id ;

SELECT * FROM v_auteur_livre 
WHERE genre_livre ='Drame'
ORDER BY auteur;

-- Supression de la vue
DROP VIEW v_auteur_livre;