USE bibliotheque;

SELECT genre,genres.nom, count(titre) ,TRUNCATE(avg(annee),0) FROM livres 
INNER JOIN genres ON livres.genre=genres.id 
GROUP BY genre;

-- Regroupement avec Group By
-- Obtenir le nombre d'auteur par pays
SELECT pays.nom,count(auteurs.id) FROM auteurs
INNER JOIN pays ON auteurs.nation=pays.id
GROUP BY pays.nom;

-- Le nombre de livre par auteur pour les auteurs qui ont écrit aumoins 3 livres
-- having -> condition (idem where) après le regroupement, Where se trouve toujours avant le regroupement
SELECT auteurs.nom, count(titre) AS nb_livre FROM auteurs
INNER JOIN livre2auteur ON auteurs.id=livre2auteur.id_auteur
INNER JOIN livres ON livres.id=livre2auteur.id_livre
GROUP BY auteurs.nom
HAVING nb_livre>3
ORDER BY nb_livre DESC;

-- Nombre de livre par genre pour le livres sortie après 1960 
-- pour les auteurs qui ont écrit au moins 10 livres
SELECT genres.nom,count(livres.genre) AS nb_livre FROM livres
INNER JOIN genres ON livres.genre=genres.id 
WHERE annee>1960
GROUP BY genre
HAVING nb_livre>10;

-- Exercice :Regroupement Group By
USE world;

--  Nombre de pays par continent
SELECT count(code), continent FROM country GROUP BY Continent ;

-- Nombre de langue officielle par pays - classé par nombre de langue officielle
SELECT name, count(CountryCode) AS nb_language FROM country
INNER JOIN countrylanguage ON code =CountryCode 
WHERE IsOfficial ='T'
GROUP BY name
ORDER BY nb_language ;

-- Liste des pays ayant au moins 2 langues officielles
SELECT name, count(CountryCode) AS nb_language FROM country
INNER JOIN countrylanguage ON code =CountryCode 
WHERE IsOfficial ='T'
GROUP BY name
HAVING nb_language>=2
ORDER BY nb_language ;

USE exemple;
-- Fonction de fenetrage

-- Clause over
-- OVER() -> la partition est la table compléte
SELECT annee,nom_vendeur ,vente,SUM (vente) OVER() FROM ventes;

-- OVER(PARTITION BY annee) -> les partitions correspondent aux annees
SELECT annee,nom_vendeur ,vente,SUM (vente) OVER(PARTITION BY annee) FROM ventes;

-- OVER(PARTITION BY nom_vendeur) -> les partitions correspondent aux nom_vendeur
SELECT annee, nom_vendeur ,vente,SUM (vente) OVER(PARTITION BY nom_vendeur) FROM ventes;

-- Agregate window function
-- count, sum,min,max, avg
SELECT annee, nom_vendeur ,vente,SUM (vente) OVER(PARTITION BY nom_vendeur) FROM ventes;

-- Ranking window fonction
-- ROW_NUMBER() -> numérote 1, 2, ... les lignes de la partitions
SELECT annee,nom_vendeur,vente ,ROW_NUMBER() OVER(PARTITION BY nom_vendeur ORDER BY vente,annee DESC) FROM ventes;

-- RANK() -> idem DENSE_RANK(), mais on aura des écarts dans la séquence des valeurs lorsque plusieurs lignes ont le même rang
-- 1 - 2 - 3 - 4 - 4 - 6
SELECT annee,nom_vendeur,vente ,RANK() OVER(PARTITION BY nom_vendeur ORDER BY vente DESC) FROM ventes;

-- DENSE_RANK() -> Attribue un rang à chaque ligne de sa partition en fonction de la clause ORDER BY
-- 1 - 2 - 3 - 4 - 4 - 5
SELECT annee,nom_vendeur,vente ,DENSE_RANK() OVER(PARTITION BY nom_vendeur ORDER BY vente DESC) FROM ventes;

-- PERCENT_RANK() -> Calculer le rang en pourcentage
SELECT annee, nom_vendeur ,vente, PERCENT_RANK() OVER(PARTITION BY nom_vendeur ORDER BY vente DESC )*100 FROM ventes;

-- NTILE(n)-> Distribue les lignes de chaque partition de fenêtre dans un nombre spécifié de groupes classés
SELECT annee,nom_vendeur,vente ,NTILE(5) OVER(PARTITION BY nom_vendeur ORDER BY vente DESC) FROM ventes;

-- Value window fonction 
-- LAG -> Renvoie la valeur de la ligne avant la ligne actuelle dans une partition
-- vente	lag(vente)
-- 16000
-- 15000	16000
SELECT annee,nom_vendeur,vente ,LAG(vente)OVER(PARTITION BY nom_vendeur ORDER  BY vente DESC) FROM ventes;

-- LEAD -> Renvoie la valeur de la ligne après la ligne actuelle dans une partition
-- vente	lead(vente)
-- 16000	5000
-- 5000		15000
SELECT annee,nom_vendeur,vente ,Lead(vente)OVER(PARTITION BY nom_vendeur ORDER  BY vente DESC) FROM ventes;

-- FIRST_VALUE ->Renvoie la valeur de l'expression spécifiée par rapport à la première ligne de la frame
SELECT annee,nom_vendeur,vente, FIRST_VALUE(vente)OVER(PARTITION BY nom_vendeur) FROM ventes;

-- LAST_VALUE -> Renvoie la valeur de l'expression spécifiée par rapport à la dernière ligne de la frame
SELECT annee,nom_vendeur,vente ,LAST_VALUE(vente)OVER(PARTITION BY nom_vendeur) FROM ventes;

-- NTH_VALUE -> Renvoie la valeur de l'argument de la nème ligne
SELECT annee,nom_vendeur,vente ,NTH_VALUE(vente,4) OVER(PARTITION BY nom_vendeur ORDER  BY vente DESC) FROM ventes;

-- CUME_DIST -> Calcule la distribution cumulée d'une valeur dans un ensemble de valeurs
SELECT annee,nom_vendeur,vente ,CUME_DIST() OVER(ORDER BY annee) FROM ventes;

-- Frame
SELECT annee, nom_vendeur, vente,
sum(vente) OVER(
				PARTITION BY annee
				ORDER BY annee
				ROWS BETWEEN CURRENT ROW AND 1 FOLLOWING
				)
FROM ventes;

-- GROUP BY WITH ROLLUP
-- La clause ROLLUP génère plusieurs ensembles de regroupement en fonction 
-- des colonnes ou des expressions spécifiées dans la clause GROUP BY
SELECT COALESCE (annee,'Total='), SUM(vente) FROM ventes
GROUP BY annee WITH ROLLUP;

SELECT annee,SUM(vente),nom_vendeur FROM ventes
GROUP BY annee,nom_vendeur WITH ROLLUP;

USE bibliotheque;

SELECT pays.nom, count(auteurs.id) FROM auteurs 
INNER JOIN pays ON auteurs.nation=pays.id
GROUP BY pays.nom WITH ROLLUP ;

-- GROUPING() NULL -> 1, sinon, elle renvoie 0
SELECT IF(grouping(auteurs.nom)=1,'Total livre',auteurs.nom),
IF(GROUPING(genres.nom)=1,'Total auteurs',genres.nom), count(livres.id) FROM genres
INNER JOIN livres ON livres.genre =genres.id
INNER JOIN livre2auteur ON livres.id=livre2auteur.id_livre
INNER JOIN auteurs ON auteurs.id = livre2auteur.id_auteur
GROUP BY auteurs.nom,genres.nom WITH ROLLUP;

-- Exercice: Group By avec WITH ROLLUP
-- Combien de mâles et de femelles de chaque race avons-nous, avec un compte total intermédiaire pour les races (mâles et femelles confondues)
SELECT Animal.sexe, Race.nom, COUNT(Animal.id) AS nombre
FROM Animal
INNER JOIN Race ON Animal.race_id = Race.id
GROUP BY Race.nom, sexe WITH ROLLUP;
