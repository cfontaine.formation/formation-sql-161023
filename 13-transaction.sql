USE exemple;

CREATE TABLE compte_bancaire(
	id INT PRIMARY KEY AUTO_INCREMENT,
	solde DECIMAL(10,3),
	iban VARCHAR(20),
	titulaire VARCHAR(50)
);

INSERT INTO compte_bancaire (solde,iban,titulaire) VALUES
(1000.000,'FR-105-0000-01','John Doe'),
(5000.000,'FR-105-0000-02','Jane Doe'),
(500.000,'FR-105-0000-03','Alan Smithee'),
(400.000,'FR-105-0000-04','Yves Roulo');

-- desactiver l'autocommit
SET autocommit 0;

START TRANSACTION;	-- Ouverture de la TRANSACTION
-- transfert entre 2 comptes bancaires
UPDATE compte_bancaire SET solde=solde-100.00 WHERE id=2;
UPDATE compte_bancaire SET solde=solde+100.00 WHERE id=3;
COMMIT; -- validation de la TRANSACTION

SELECT * FROM compte_bancaire;

START TRANSACTION;
UPDATE compte_bancaire SET solde=solde-50.00 WHERE id=1;
UPDATE compte_bancaire SET solde=solde+50.00 WHERE id=2;
ROLLBACK;  -- annulation de transaction

SELECT * FROM compte_bancaire;

-- Point de sauvegarde
START TRANSACTION;	-- Ouverture de la TRANSACTION
UPDATE compte_bancaire SET solde=solde-20.00 WHERE id=1;
UPDATE compte_bancaire SET solde=solde+20.00 WHERE id=2;
SAVEPOINT transfert1;	-- Création d'un point de sauvegarde
INSERT INTO compte_bancaire (solde,iban,titulaire) VALUES 
(100.0,'FR-105-0000-05','Antoine beretto');
ROLLBACK TO transfert1 ; -- Retour au point de sauvegarde transfert1
INSERT INTO compte_bancaire (solde,iban,titulaire) VALUES 
(6000.0,'FR-105-0000-05','Antoine beretto');
SAVEPOINT creation_compte_beretto;
UPDATE compte_bancaire SET solde=solde-200.00 WHERE id=6;
UPDATE compte_bancaire SET solde=solde+200.00 WHERE id=3;

-- Libération des points de sauvegarde
RELEASE SAVEPOINT transfert1;	
RELEASE  SAVEPOINT creation_compte_beretto;
COMMIT;

SELECT * FROM compte_bancaire;

-- activation de l'autocommit
SET autocommit =1;



