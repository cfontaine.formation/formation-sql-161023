-- Déclaration d'un procedure stockée
DELIMITER $ -- CHANGE le délimiteur de fin de ligne ; -> $
CREATE PROCEDURE auteur_vivant()
BEGIN
	SELECT prenom,nom,naissance FROM auteurs WHERE deces IS NULL; 
END $
DELIMITER ; -- CHANGE le délimiteur de fin de ligne $ -> ;

DELIMITER $
CREATE PROCEDURE test_variable()
BEGIN 
	-- déclaration d'une variable locale
	DECLARE var_local INT DEFAULT 10;
	-- affecter une valeur à une variable locale
	SET var_local=15;
	-- afficher une variable
	SELECT var_local;
	-- INTO -> affecter une variable avec le résultat d'une requête
	SELECT count(id) INTO var_local FROM genres;
	SELECT var_local;
	SELECT @var_global;
END $
DELIMITER ;

-- Passage de paramètre
-- - en entrée 2 entiers a et b
-- - en sortie un entier somme 
DELIMITER $
CREATE PROCEDURE addition(IN a INT,IN b INT,OUT somme INT)
BEGIN
	SET somme= a + b;
END $
DELIMITER ;

-- nb_livre_genre -> la procédure stockée prend en paramètre l'id du genre 
-- et en sortie on obtient le nombre de livre qui correspond à ce genre  
DELIMITER $
CREATE PROCEDURE nb_livre_genre(IN id_genre INT,OUT nb_livre INT)
BEGIN
	SELECT count(livres.id) INTO nb_livre FROM livres WHERE livres.genre=id_genre;
END $	
DELIMITER ;

-- Exercice Procédure Stockée
-- Créer une procédure qui a pour nom: nb_ville_pays en paramètre, on a:
--  - en entrée un entier code_country
--  - en sortie un entier nb_ville_pays
-- La procédure pour le code_country passé en paramètre va retourner le nombre de ville
DELIMITER $
CREATE PROCEDURE nb_ville_pays( IN code_country CHAR(3), OUT nb_ville_pays INT)
BEGIN
SELECT count(id) INTO nb_ville_pays FROM city WHERE countrycode=code_country COLLATE utf8mb4_general_ci;
END $
DELIMITER ;

-- Condition IF
-- nb_livre_genre2 -> la procedure prend en paramètre le nom du genre
-- en resultat on obtient:
-- - si le nom du genre n'existe pas dans la genres -> NULL
-- - si le nom existe -> le nombre de livre qui correspond à ce genre
DELIMITER $
CREATE PROCEDURE nb_livre_genre2(IN genre VARCHAR(30),OUT nb_livre INT)
BEGIN 
	DECLARE id_genre INT;
	SELECT id INTO id_genre FROM genres WHERE nom=genre COLLATE utf8mb4_general_ci;
	IF id_genre IS NOT NULL THEN
		SELECT count(livres.id) INTO nb_livre FROM livres WHERE livres.genre=id_genre;
	ELSE
		SET nb_livre=NULL;
	END IF;
END $
DELIMITER ;

-- Condition Case
DELIMITER $
CREATE PROCEDURE categorie_age_livre(IN annee INT, OUT categorie_age VARCHAR(50))
BEGIN
	DECLARE age_livre INT;
	IF annee<YEAR(now()) THEN 
		SET age_livre=YEAR(now())-annee;
		CASE 
			WHEN annee>2000 THEN SET categorie_age= concat('Moderne (',age_livre,' ans)');
			WHEN annee<2000 AND annee>1900 THEN SET categorie_age= concat('20 ème siècle (',age_livre,' ans)');
		ELSE 
			 SET categorie_age= concat('Ancien (',age_livre,' ans)');
		END CASE;
	ELSE
		SET categorie_age='ERREUR';
	END IF;
END $
DELIMITER ;


-- Boucle while
DELIMITER $
CREATE PROCEDURE somme_multi(IN valeur INT,OUT resultat INT)
BEGIN 
	DECLARE i INT DEFAULT 0;
	SET resultat=0;
	WHILE i<valeur DO
		SET i=i+1;
		SET resultat=resultat+i;
	END WHILE;	
END $
DELIMITER ;

-- Utilistion d'un curseur pour parcourir un résultat
DELIMITER $
CREATE PROCEDURE auteurs_pays(IN id_pays INT,OUT auteurs VARCHAR(10000))
BEGIN 
	DECLARE done INT DEFAULT 0;
	DECLARE auth VARCHAR(100);
	-- Déclaration du curseur 
	DECLARE cur_auteur CURSOR FOR SELECT concat(prenom, ' ', nom ) FROM auteurs WHERE nation=id_pays;
	-- Lorsque le curseur arrive en fin du résultat  la variable done est affectée à 1
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done=1;
	SET auteurs='';
	OPEN cur_auteur; -- Ouverture du curseur
		WHILE done=0 DO 
			FETCH cur_auteur INTO auth; -- le nom de l'auteur est placé dans a variable auth 
										-- le curseur passe au prochain résultat
			SET auteurs= concat_ws(', ',auteurs,auth);
		END WHILE;
	CLOSE cur_auteur; -- Fermeture du curseur
END $
DELIMITER ;