USE exemple; 

-- Ajouter des données
-- Insérer une ligne en spécifiant toutes les colonnes
INSERT INTO marques VALUES (1,'Marque A','1983-03-21');

-- Insérer une ligne en spécifiant les colonnes souhaitées
INSERT INTO marques(nom, date_creation) VALUES('Marques B','1978-05-03');
INSERT INTO marques(nom) VALUES('Marque C');

-- Insérer Plusieurs lignes
INSERT INTO articles(description,nom_article,prix,marque) VALUES 
('Une télévision 4K','TV 4K',650.0,2),
('Disque SSD 2go','Disque SSD2G',99.0,1),
('Souris sans fils','souris',30.0,1);

-- Erreur => La marque avec l'id 20 n'existe pas, la contrainte d'intégritée référentielle, n'est pas respecté
-- INSERT INTO articles(description,nom,prix,marque) VALUES ('TV 4K','TV 4K100hz',600.0,20);

-- Insérer des données dans une relation n-n
INSERT INTO fournisseurs(nom) VALUES
('fournisseur 1'),
('fournisseur 2');

INSERT INTO articles_founisseurs(id_article,id_fournisseur) VALUES 
(1,1),
(3,1),
(2,2),
(3,2);

-- Supprimer des données => DELETE, TRUNCATE
-- Supression de la ligne qui a pour id 3 dans la table marques
DELETE FROM marques WHERE id=3;

-- Supprimer toutes les lignes de la table articles_fournisseurs
DELETE FROM articles_founisseurs ;

-- Supprimer toutes les lignes de la table qui ont un prix <100.0
DELETE FROM articles WHERE prix<100.0;

-- Valeur auto_increment et la suppression
-- DELETE => ne ré-initialise pas la colonne AUTO_INCREMENT
DELETE FROM articles;
INSERT INTO articles(description,nom_article,prix,marque) VALUES 
('Une télévision 4K','TV 4K',650.0,2); -- id=7

-- TRUNCATE => réinitailise l'auto incrément
SET FOREIGN_KEY_CHECKS=0;	-- désactiver la vérification des clés étrangères (MYSQL/ MARIADB)
-- Supression de toutes les lignes de la table articles  => remet la colonne AUTO_INCREMENT à 0
TRUNCATE articles;
SET FOREIGN_KEY_CHECKS=1;	-- réactiver la vérification des clés étrangères

INSERT INTO articles(description,nom_article,prix,marque) VALUES 
('Une télévision 4K','TV 4K',650.0,2); -- id=1

INSERT INTO articles(description,nom_article,prix,marque) VALUES 
('Disque SSD 2go','Disque SSD2G',99.0,1),
('Souris sans fils','souris',30.0,1);

-- Modification des données => UPDATE
-- Pas de condition tous les prix passe à 1.0
UPDATE articles SET prix=1;

-- Modification du prix de l'article qui a pour reference 1
UPDATE articles SET prix=660 WHERE reference =1;

-- Modification du prix et de la marque de l'article qui a pour reference 5 
UPDATE articles SET prix=99,marque=2 WHERE reference=5;

-- Modifier tous les prix supérieur à 30 en les augmentant de 10%
UPDATE articles SET prix=prix*1.10 WHERE prix>30.0;

USE pizzeria;
INSERT INTO pizzas(nom,base,photo)
VALUES('pizza forrestière','rouge',LOAD_FILE('C:/Dawan/Formations/pizza.jpg'));
