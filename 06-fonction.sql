-- Syntaxe d'une fonction
-- nom_fonction(param1,param2 ...)

-- Fonctions arithmétiques
-- CEIL(1.2) -> 2, FLOOR(1.6) -> 1, ROUND(1.4628476,3) -> 1.463, TRUNCATE(1.4628476,3) -> 1,462
-- RAND() -> nombre aléatoire entre 0 et 1
SELECT CEIL(1.2) AS CEIL, FLOOR(1.6) AS FLOOR, ROUND(1.4628476,3) AS ROUND,  TRUNCATE(1.4628476,3) AS TRUNCATE,RAND();

USE bibliotheque;
-- On peut utiliser rand() pour sélectionner aléatoirement 5 livres 
SELECT titre FROM livres ORDER BY rand() LIMIT 5;

-- Fonction Chaine de caractère
-- - Taille de la chaine de caractère nom en octets
-- - Nombre de caractère de la chaine de caractère nom 
SELECT nom, LENGTH(nom) AS nb_octet,char_length(nom) AS nb_caractere FROM auteurs;

-- On ne sélectionne que les lignes dont la nom fait plus de 7 caratères 
SELECT nom, LENGTH(nom) AS nb_octet,char_length(nom) AS nb_caractere FROM auteurs
WHERE char_length(nom)>7;

-- a-> 97 , b -> 98, A -> 64
SELECT ascii('a'),ascii('b'),ascii('A');

-- concat -> Concaténation de chaînes
SELECT CONCAT(prenom,' ', auteurs.nom,' (',pays.nom,')') AS auteur FROM auteurs
INNER JOIN pays ON pays.id=nation;

-- concat_ws -> Concaténation de chaînes avec un séparateur (->1er paramètre) 
SELECT CONCAT_WS(',',prenom,nom,naissance) FROM auteurs;

-- space(20) -> retourne une chaîne contenant 20 espaces
SELECT CONCAT('|',SPACE(20),'|');

-- insert: Insertion d'une chaîne à une position pos et pour num caractères
-- Insertion d'une chaîne ---- au 3 caractères et pour remplcer 2 caractères
-- James -> Ja----s
SELECT prenom, insert(prenom,3,2,'---') FROM auteurs;

-- remplace: Remplace toutes les occurrences d'une sous-chaîne par une nouvelle chaîne
-- remplace dans le nom er par ---
-- Hebert -> H---b---t
SELECT nom, REPLACE(nom,'er','---') FROM auteurs;

-- repeat -> répéte le nom 4 fois
-- reverse -> Inverse les caractères du nom (Ellroy -> yorllE)
SELECT repeat(nom,4),reverse(nom) FROM auteurs;

-- left(prenom,3) -> Extrait 3 caractère en partant de la gauche du prénom
-- right(prenom,3) -> Extrait 3 caractère en partant de la droite du prénom
-- substr(prenom,2,3) -> Extraction d'une sous chaîne du prénom à partir du 2ème caractères et pour 3 caractères
SELECT prenom,LEFT(prenom,3),RIGHT(prenom,3),SUBSTR(prenom,2,3) FROM auteurs;

-- Renvoie la position de la première occurrence de la chaîne 'am', dans le prenom
SELECT prenom, position('am' IN prenom) FROM auteurs;

-- FIND_IN_SET Renvoie la position de la chaîne aze dans la chaîne 'ert,vkkrgh,aze,rty'
-- contenant une liste de sous-chaîne séparé par une virgule -> 3
SELECT FIND_IN_SET('aze','ert,vkkrgh,aze,rty');

-- FIELD Renvoie la position de 'zse' dans la liste de valeur 'aze','uio','zse','123','rty'
SELECT FIELD ('zse','aze','zse','123','rty');

-- LTRIM -> Retire les caractère blanc à gauche de la chaîne
-- RTRIM -> Retire les caractère blanc à droite de la chaîne
-- TRIM ->Retire les caractère blanc à droite et à gauche
SELECT LTRIM('           HELLO WORLD'), RTRIM('HELLO WORLD               '),TRIM('           HELLO WORLD          ');
SELECT LTRIM(prenom) FROM auteurs;

-- lower(prenom) -> Conversion en minuscule du prénom
-- upper(prenom) -> Conversion en majuscule du prénom
SELECT lower(prenom),upper(nom) FROM auteurs;

-- 4 chiffres après la virgule 1,970.0000
SELECT format(annee,4) FROM livres;

-- strcmp -> Compare 2 chaînes : 
-- 'hello' = 'hello' → 0 
-- 'bonjour' < 'hello' → -1
-- 'bonjour' > 'hello' → 1
SELECT STRCMP('bonjour','hello'), STRCMP('hello','bonjour'),STRCMP('hello','hello');

-- Fonction Temporelle
-- CURRENT_DATE() -> Date courante
-- CURRENT_TIME() -> Heure courante
-- CURRENT_TIMESTAMP() ou NOW() -> Date et heure courante
SELECT CURRENT_date(),CURRENT_TIME(),CURRENT_TIMESTAMP(),NOW();

-- DATE -> Extrait la date de la Date et de l'heure courante -> 2023-10-18
-- Day -> Extrait le jour de la Date et de l'heure courante -> 18
SELECT DATE(NOW()),Day(NOW());

-- MONTH -> Extrait le mois -> 10
-- YEAR -> Extrait l'année -> 2023
-- dayofmonth -> Extrait le jour -> 20
-- quarter -> retourne le trimestre d'une date -> 3
-- WEEK -> Numéro de semaine d'une date (0 à 53)
SELECT nom,naissance, MONTH(naissance),YEAR(naissance),dayofmonth(naissance),quarter(naissance),WEEK(naissance) FROM auteurs;

-- dayname -> Extrait le nom du jour de la semaine  -> Wednesday
-- monthname -> Extrait le nom du mois -> October
-- last_day -> Le dernier jour du mois (30,31,29,28)
-- DAYOFYEAR -> Le jour de l'année (1 à 366)
SELECT nom,naissance,dayname(naissance),monthname(naissance),Last_Day(naissance),DAYOFYEAR(naissance)FROM auteurs; 

-- dayofweek -> Le numéro du jour de la semaine d'une date (1→ dimanche … 7→ samedi)
-- weekday -> Numéro de jour de la semaine (0 → lundi … 6 → dimanche)
SELECT dayofweek(naissance),weekday(naissance) FROM auteurs;

-- Nombre de jour entre 2 dates
SELECT DATEDIFF('2023-12-31',current_date());

-- ADDDATE -> Ajoute un intervalle à une date
-- SUBDATE -> Soustrait un intervalle à une date
SELECT DATE_ADD(current_date(),INTERVAL 4 MONTH),DATE_SUB(current_date(),INTERVAL 3 YEAR);

-- DATE_FORMAT -> Formater une date
SELECT DATE_FORMAT(naissance,'%m %Y (%j)') FROM auteurs;

-- STR_TO_DATE -> Convertir une chaîne en date suivant un format
SELECT STR_TO_DATE('10/05/2015','%d/%m/%Y');

-- L'heure
-- HOUR -> extraire les heures
-- MINUTE -> extraire les minutes
-- SECOND -> extraire les secondes
-- microsecond -> extraire les microsecondes
SELECT HOUR(current_time()),MINUTE(current_time()),SECOND(current_time()),microsecond(current_time()) ;

-- timediff -> Différence entre 2 heures
-- subtime -> Soustrait un intervalle à une heure
-- addtime -> Ajoute un intervalle à une heure
SELECT timediff('12:30:00',current_time()),subtime(current_time(),'05:00:00'),addtime(current_time(),'05:00:00');

-- time_to_sec -> Conversion d’une heure en seconde
SELECT time_to_sec(current_time());

-- TIME_FORMAT -> Formater une heure suivant un format
SELECT TIME_FORMAT(current_time(),'%l:%i:%s %p');

-- Fonction d'agrégation
SELECT count(id),min(annee),max(annee),truncate(avg(annee),0) FROM livres

-- Autre fonction
-- current_user -> utilisateur courant
-- DATABASE () -> base de donnée courante
-- VERSION() -> version de la sgbd
-- last_insert_id() -> L’id AUTO_INCREMENT de la dernière ligne qui a été insérée ou modifiée
SELECT current_user(),DATABASE (),VERSION(),last_insert_id(); 

-- COALESCE -> Retourne la première valeur non-null de la liste
SELECT COALESCE (NULL,NULL,'az',NULL,5);

-- NULLIF -> Retourne NULL si les 2 paramètres sont égaux sinon retourne le premier paramètre
SELECT annee, NULLIF(annee,1954) FROM livres;

USE exemple;

-- Fonction if
CREATE TABLE comptes(
	id INT PRIMARY KEY AUTO_INCREMENT,
	valeur DECIMAL(8,2),
	nom VARCHAR(40),
	sens CHAR(1)
);

INSERT INTO comptes(valeur,nom, sens) VALUES
(300,'','+'),
(400,'','-'),
(250,'','-'),
(500,'','+');

SELECT IF(sens='-',-valeur,valeur), sens FROM comptes;

USE bibliotheque;

-- Fonction CASE
SELECT annee,titre,
CASE 
	WHEN annee>2000 THEN 'Moderne'
	WHEN annee<=2000 AND annee>1900 THEN '20 eme siecle'
	ELSE 'Livre ancien'
END FROM livres;

SELECT titre,genre,
CASE(genre)
	WHEN 1 THEN'Roman policier'
	WHEN 6 THEN 'Roman Science-fiction'
	WHEN 7 THEN 'Drame'
	ELSE 'Une autre catégorie'
END 
FROM livres;
END

USE world;
-- Nombre de pays présents dans la table Country
SELECT count(code) FROM country;

-- Écrire une requête pour générer un code qui a pour forme les 3 première lettres de la ville concaténé avec la chaine '0000'et le nombre de caractère de la ville et séparé par 
SELECT concat_ws('-',left(name ,3),'0000',char_length(name)) AS Code_City FROM city;

USE elevage;
-- Afficher le nombre de race dans la table Race
SELECT count(id) FROM race;

-- Afficher la liste des animaux nés en 2006
SELECT nom,date_naissance FROM animal WHERE YEAR(date_naissance)=2006;

-- Afficher le nom  de tous les animaux nés dans les huit premières semaines d'une année.
SELECT nom,date_naissance FROM animal WHERE WEEK(date_naissance)<=8;

-- Afficher les chats dont la deuxième lettre du nom est un a
SELECT animal.nom, espece.nom_courant FROM animal 
INNER JOIN espece ON animal.espece_id=espece.id 
WHERE espece.nom_courant = 'chat' AND substr(nom,2,1)='a';

-- Afficher les chiens dont le nom a un nombre pair de lettres
SELECT animal.nom, espece.nom_courant FROM animal 
INNER JOIN espece ON animal.espece_id=espece.id 
WHERE espece.nom_courant = 'chien' AND char_length(animal.nom)%2=0;

-- Afficher le nombre de chiens dont on connait le père
SELECT COUNT(pere_id)
FROM Animal
INNER JOIN Espece ON Espece.id = Animal.espece_id
WHERE Espece.nom_courant = 'Chien';