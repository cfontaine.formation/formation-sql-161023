USE bibliotheque;

-- Jointure Interne 
-- Avec WHERE
-- Afficher le titre, l'annee et le genre du livre
SELECT titre, annee,genre, genres.id, nom FROM livres,genres
WHERE livres.genre=genres.id;

-- INNER JOIN
-- Relation 1,N

-- On va joindre la table livres et la table genres en utilisant l'égalité entre
-- La clé primaire de genres -> id et la clé étrangère de livre -> genre
SELECT titre, annee, nom FROM livres
INNER JOIN genres ON livres.genre = genres.id ;

-- Afficher le prénom et le nom de l'auteur et le nom du pays
SELECT prenom,auteurs.nom,pays.nom FROM auteurs
INNER JOIN pays ON nation=pays.id;

-- Afficher le titre, l'année de sortie et le genre du livre qui sont sortie entre 1960 et 1970
-- trié par année croissante
SELECT titre, annee, nom AS categorie FROM livres
INNER JOIN genres ON livres.genre = genres.id 
WHERE annee BETWEEN 1960 AND 1970 ORDER BY annee;

-- Afficher le titre, l'année de sortie et le genre du livre pour les livres qui sont des livres policier ou fantastique 
-- trié par genre et par année croissante
SELECT titre, annee, nom AS categorie FROM livres
INNER JOIN genres ON livres.genre = genres.id
WHERE nom IN ('Policier','Fantastique') ORDER BY nom , annee;

-- Relation N,N
-- On va joindre : la table livres et la table de joiture livre2auteur
-- On va joindre le resultat de la jointure précédente avec la table auteurs
-- Afficher le titre du livre et le prénom et le nom de son auteurs
SELECT titre,annee, prenom,nom FROM auteurs
INNER JOIN livre2auteur ON auteurs.id=livre2auteur.id_auteur 
INNER JOIN livres ON livres.id = livre2auteur.id_livre;

-- Afficher le titre, l'année de sortie, le genre, le prenom et le nom 
SELECT titre,annee, genres.nom, prenom,auteurs.nom FROM auteurs
INNER JOIN livre2auteur ON auteurs.id=livre2auteur.id_auteur 
INNER JOIN livres ON livres.id = livre2auteur.id_livre
INNER JOIN genres ON livres.genre = genres.id;

-- Afficher le titre,l'année de sortie, le genre, le prenom, le nom et la nationalité pour les auteurs Français ou belge
SELECT titre,annee, genres.nom, prenom,auteurs.nom,pays.nom FROM auteurs
INNER JOIN livre2auteur ON auteurs.id=livre2auteur.id_auteur 
INNER JOIN livres ON livres.id = livre2auteur.id_livre
INNER JOIN genres ON livres.genre = genres.id
INNER JOIN pays ON nation=pays.id 
WHERE pays.nom IN ('belgique','france') AND genres.nom='Policier';

-- Produit cartésien -> CROSS JOIN
-- On obtient toutes les combinaisons possible entre les 2 tables
-- Le nombre d'enregistrements obtenu est égal au nombre de lignes de la première table multiplié par le nombre de lignes de la deuxième table.
USE exemple;
CREATE TABLE plats(
	id INT PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(40)
);

CREATE TABLE boissons(
	id INT PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(40)
);

INSERT INTO plats(nom) VALUES
('Céréale'),
('Pain'),
('oeuf sur le plat');

INSERT INTO boissons(nom) VALUES
('Café'),
('Thé'),
('Jus d\'orange');

-- On obtient toutes les combinaisons possible entre les boissons et les plats
SELECT plats.nom AS plat, boissons.nom AS boisson FROM plats 
CROSS JOIN boissons;

USE world;
-- Afficher chaque nom de pays et le nom de sa capitale
SELECT country.name, city.name FROM country
INNER JOIN city ON country.capital=city.id;

-- Afficher les nom de pays et le nom de ses villes classer par nom de pays puis par nom de ville en ordre alphabétique (ne pas afficher les pays sans ville)
SELECT country.name, city.name FROM country
INNER JOIN city ON country.Code = city.CountryCode
ORDER BY country.name, city.name;

-- Afficher: les noms de  pays, la  langue et le  pourcentage  classé par pays et par  pourcentage décroissant
SELECT country.name, countrylanguage.language, countrylanguage.Percentage FROM country
INNER JOIN countrylanguage ON countrylanguage.CountryCode = country.Code 
ORDER BY country.name,countrylanguage.Percentage DESC;

-- idem, mais on ne veut obtenir que les langues officielles
SELECT country.name, countrylanguage.language, countrylanguage.Percentage FROM country
INNER JOIN countrylanguage ON countrylanguage.CountryCode = country.Code 
WHERE countrylanguage.IsOfficial='T' 
ORDER BY country.name,countrylanguage.Percentage DESC;

USE bibliotheque;

SELECT nom FROM genres
LEFT JOIN livres
ON genres.id=livres.genre
WHERE livres.id IS NULL;

-- INSERT INTO bibliotheque.auteurs (id, prenom, nom, naissance, deces, nation) VALUES(37, 'Honoré', 'de Balzac', '1799-05-20', '1850-08-18', NULL);
-- INSERT INTO bibliotheque.auteurs (id, prenom, nom, naissance, deces, nation) VALUES(38, 'Guy', 'de Maupassant', '1850-08-05', '1893-07-06', NULL);
-- INSERT INTO bibliotheque.auteurs (id, prenom, nom, naissance, deces, nation) VALUES(39, 'Roberto', 'Saviano', '1979-09-22', NULL, NULL);
-- 
-- INSERT INTO pays(nom) VALUES('Portugal'),('Bolivie'),('Chine');

-- Jointure externe: LEFT JOIN ou RIGTH JOIN
SELECT prenom,auteurs.nom AS pays FROM auteurs
LEFT JOIN pays
ON pays.id= nation
WHERE pays.id IS NULL;

SELECT pays.nom AS pays FROM auteurs
RIGHT  JOIN pays
ON pays.id= nation
WHERE auteurs.id IS NULL;

-- SELECT prenom,auteurs.nom AS nom_auteur,pays.nom AS pays FROM auteurs
-- FULL JOIN pays
-- ON pays.id=nation;

-- FULL JOIN n'est pas encore supporté par MYsql. Mais on peut le réaliser avec cette requète.
SELECT prenom,auteurs.nom AS nom_auteur,pays.nom  FROM auteurs
LEFT JOIN pays
ON pays.id= nation
UNION
SELECT prenom,auteurs.nom AS nom_auteur,pays.nom  FROM auteurs
RIGHT  JOIN pays
ON pays.id= nation;

-- Jointure naturelle (MySQL/ MariaDB)
-- Il faut que la clé primaire et la clé étrangère doivent avoir le même nom

-- Changer le nom de la clé primaire  id -> genre
ALTER TABLE livres 
DROP CONSTRAINT fk_genres;

ALTER TABLE genres CHANGE id genre INT;

ALTER TABLE livres 
ADD CONSTRAINT fk_genres
FOREIGN KEY (genre)
REFERENCES genres(genre);

-- Jointure naturelle
SELECT titre, nom FROM livres
NATURAL JOIN genres;

-- Changer le nom de la clé primaire genre -> id
ALTER TABLE livres 
DROP CONSTRAINT fk_genres;

ALTER TABLE genres CHANGE genre id INT;

ALTER TABLE livres 
ADD CONSTRAINT fk_genres
FOREIGN KEY (genre)
REFERENCES genres(genre);

-- Auto jointure
-- Une table que l'on joindre avec elle même

-- Une auto joiture est souvent utilisé pour représenter
-- une hierarchie en SQL (relation parent->enfant)
USE exemple;
CREATE TABLE salaries(
	id INT PRIMARY KEY AUTO_INCREMENT,
	prenom VARCHAR(40),
	nom VARCHAR(40),
	manager INT,
	
	CONSTRAINT FK_manager
	FOREIGN KEY (manager)
	REFERENCES salaries(id)
);

INSERT INTO salaries(prenom,nom,manager) VALUES 
('John','Doe',NULL),
('Alan','Smithee',1),
('yves','dupont',1),
('joe','dalton',NULL),
('Jane','Doe',4);

SELECT employe.prenom AS emp_prenom,employe.nom AS emp_nom,manager.prenom AS mg_prenom, manager.nom AS mg_nom
FROM salaries AS employe
LEFT JOIN salaries AS manager 
ON employe.manager = manager.id;

-- Exercice: Jointure interne et externe
USE world;

-- Afficher le nom des pays sans ville
SELECT country.name AS pays 
FROM country
LEFT JOIN city
ON country.code = city.countrycode
WHERE city.name IS NULL;

-- Afficher tous les pays qui parlent français
SELECT country.name AS pays_francophone 
FROM country 
INNER JOIN countrylanguage
ON country.code =countrylanguage.countrycode
WHERE countryLanguage.language='French';

