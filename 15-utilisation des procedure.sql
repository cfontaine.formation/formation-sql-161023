USE bibliotheque;

-- Appel de la procedure stockée
CALL auteur_vivant(); 

-- Afficher la liste des procédures stockées
SHOW PROCEDURE STATUS ;

-- @var_global --> variable globale
-- Affecter une valeur à une variable globale
SET @var_global = 'test';

-- Afficher le contenu d'une variable 
SELECT @var_global;

CALL test_variable();
-- SELECT var_local; -- on ne peut accèder à une variable locale en dehors la procédure où elle est déclarée

-- Exemple passage de paramètre
-- 10 et 32 ->  paramètres en entrée
-- le résultat de l'addition va être placé dans la variable globale @var_global
CALL addition(10,32,@var_global);
SELECT @var_global; 

CALL nb_livre_genre(1,@var_global) ;
SELECT @var_global; 

-- Exercice: procédure stockée
USE world;
CALL nb_ville_pays('NLD',@var_global);
SELECT @var_global; 

USE Bibliotheque;

-- Exemple de condition IF
CALL nb_livre_genre2('Policier',@var_global) ;
SELECT @var_global; 

-- Exemple de condition CASE
CALL categorie_age_livre(3000,@var_global);
SELECT @var_global;

-- Exemple boucle WHILE
CALL somme_multi(30,@var_global);
SELECT @var_global;

-- Exemple Curseur
CALL auteurs_pays(6,@var_global);
SELECT @var_global;

-- Déclencheur
-- INSERT INTO genres(nom) VALUES('a'); -> Erreur: le nom du genre est < à 3 caratères
INSERT INTO genres(nom) VALUES('Théatre'); -- OK 
-- INSERT INTO genres(nom) VALUES('Théatre'); -> Erreur: le nom du genre existe déjà

SELECT * FROM genres;

-- Supression du déclencheur
DROP TRIGGER valide_genre;

-- Supression de toutes les procedures stockées
DROP PROCEDURE auteur_vivant;
DROP PROCEDURE test_variable;
DROP PROCEDURE addition;
DROP PROCEDURE nb_ville_pays ;