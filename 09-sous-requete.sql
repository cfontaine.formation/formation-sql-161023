USE bibliotheque;

-- requête imbriquée qui retourne un seul résultat
SELECT titre,annee FROM livres 
WHERE annee=(
			SELECT round(avg(annee),0) FROM livres WHERE genre=1
);

-- requête imbriquée qui retourne plusieurs résultat -> IN
SELECT titre, annee FROM livres
WHERE annee IN (
	SELECT annee FROM livres 
	INNER JOIN genres ON livres.genre=genres.id
	WHERE genres.nom='horreur'
);

-- requête imbriquée qui retourne plusieurs résultat ->EXISTS
SELECT genres.id ,genres.nom FROM genres 
WHERE EXISTS (
	SELECT livres.id FROM livres WHERE genre=genres.id
);

SELECT genres.id ,genres.nom FROM genres 
WHERE NOT EXISTS (
	SELECT livres.id FROM livres WHERE genre=genres.id
);

-- requête imbriquée qui retourne plusieurs résultats ->ALL
SELECT titre,annee FROM livres 
WHERE annee!= ALL(
	SELECT annee FROM livres
	INNER JOIN genres ON livres.genre=genres.id 
	WHERE genres.nom='Science-fiction'
);

-- requête imbriquée qui retourne plusieurs résultats ->ANY ou SOME
SELECT titre,annee FROM livres 
WHERE annee = ANY(
	SELECT annee FROM livres
	INNER JOIN genres ON livres.genre=genres.id 
	WHERE genres.nom='Science-fiction'
);

-- Exercise

USE world;
-- tous les pays avec une capitale de plus d'un million d'habitant

SELECT country.name AS Pays 
FROM country 
WHERE Capital IN (
	SELECT city.ID FROM city WHERE city.population > 1000000
);


-- Liste des pays ayant une langue parlée par moins de 10% de la population
SELECT country.name as Pays
FROM country
WHERE code IN (
    SELECT countrylanguage.CountryCode
    FROM countrylanguage
    WHERE countrylanguage.Percentage <10
);

USE  Elevage;

SELECT Animal.nom FROM Animal
	WHERE espece_id = ( 
	 SELECT id FROM Espece
	 WHERE nom_courant LIKE 'tor%'
);

-- Afficher le nom de la race pour lequel on a des animaux dans l'élevage
SELECT Race.id,Race.nom FROM Race
WHERE EXISTS (
		SELECT id FROM Animal 
		WHERE Animal.race_id = Race.id);

	SELECT id, nom, espece_id
FROM Animal
WHERE espece_id = ANY ( -- ANY = au moins une valeur
    SELECT id 
    FROM Espece
    WHERE nom_courant IN ('Tortue d''Hermann', 'Perroquet amazone')
);