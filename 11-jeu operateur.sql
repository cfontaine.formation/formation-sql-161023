USE exemple;

-- Union
SELECT prenom,nom,adresse_personnel,'emp' AS identifiant FROM employees
UNION
SELECT prenom,nom,adresse_personnel,'cli' AS identifiant FROM clients;

-- Pas de doublon avec union
SELECT prenom,nom,adresse_personnel FROM employees
UNION
SELECT prenom,nom,adresse_personnel FROM clients;

-- Union ALL autorise les doublon
SELECT prenom,nom,adresse_personnel FROM employees
UNION ALL
SELECT prenom,nom,adresse_personnel FROM clients;

-- Intersection
SELECT prenom,nom,adresse_personnel FROM employees
INTERSECT
SELECT prenom,nom,adresse_personnel FROM clients;

-- Except
SELECT prenom,nom,adresse_personnel FROM employees
EXCEPT
SELECT prenom,nom,adresse_personnel FROM clients;

-- Exercice: Opérateurs de jeux
USE world;

-- Afficher la liste des pays et des villes ayant moins de 1000 habitant
SELECT name FROM country
WHERE country.population<1000
UNION
SELECT name FROM city 
WHERE city.population<1000;

-- Liste des pays où on parle français ou anglais
SELECT name FROM country
INNER JOIN countrylanguage ON country.code=countrylanguage.countrycode
WHERE language='French'
UNION
SELECT name FROM country
INNER JOIN countrylanguage ON country.code=countrylanguage.countrycode
WHERE language='English';